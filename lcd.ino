


  
//LCD1602
//You should now see your LCD1602 display the flowing characters "SUNFOUNDER" and "hello, world"
//Email:support@sunfounder.com
//Website:www.sunfounder.com
//2015.5.7 
#include <LiquidCrystal.h>// include the library code
#include <dht.h>
 

 
#define dht_apin A1 // Analog Pin sensor is connected to
#define micPin A5
 
dht DHT;
 
/**********************************************************/
char array1[]=" SUNFOUNDER               ";  //the string to print on the LCD
char array2[]="hello, world!             ";  //the string to print on the LCD
int tim = 250;  //the value of delay time
// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(4, 6, 10, 11, 12, 13);
/*********************************************************/
void setup()
{
  lcd.begin(16, 2);  // set up the LCD's number of columns and rows: 
    Serial.begin(9600);
}
/*********************************************************/
void readTemp(){
  DHT.read11(dht_apin);
}

void drawLCD(){
    lcd.clear();
    lcd.setCursor(0,0);  // set the cursor to column 15, line 0
    char temp1[] = "Temp: ";
    char temp2[] = "c H: ";
    char temp3[] = "%           ";
    char data2[] = "Sound: ";
    char data3[] = "             ";
    char tmp[3]; //temperature
    char hum[3]; //humidity
    char sound[5];

    int soundValue = analogRead(micPin);
    String(DHT.temperature, 2).toCharArray(tmp, 3);
    String(DHT.humidity, 2).toCharArray(hum, 3);
    String(soundValue, 4).toCharArray(sound, 5);
    
Serial.print(temp1);
Serial.print(tmp);
Serial.print(temp2);
Serial.print(hum);
Serial.println(temp3);
Serial.print(data2);
Serial.print(sound);
Serial.println(data3);
    
    drawMsg(temp1, 6);
    drawMsg(tmp, 2); //no need to draw null terminators
    drawMsg(temp2, 5);
    drawMsg(hum, 2);
    drawMsg(temp3, 12);

    lcd.setCursor(0, 1);

    drawMsg(data2, 7);
    drawMsg(sound, 4);
    drawMsg(data3, 13);
    /*for ( int positionCounter1 = 0; positionCounter1 < 26; positionCounter1++)
    {
      lcd.scrollDisplayLeft();  //Scrolls the contents of the display one space to the left.
      lcd.print(data[positionCounter1]);  // Print a message to the LCD.
      delay(tim);  //wait for 250 microseconds
    }*/
    //lcd.clear();  //Clears the LCD screen and positions the cursor in the upper-left corner.
    //lcd.setCursor(0,1);  // set the cursor to column 15, line 1
    /*
    for (int positionCounter2 = 0; positionCounter2 < 26; positionCounter2++)
    {
      lcd.scrollDisplayLeft();  //Scrolls the contents of the display one space to the left.
      lcd.print(data2[positionCounter2]);  // Print a message to the LCD.
      delay(tim);  //wait for 250 microseconds
    }*/
    //lcd.clear();  //Clears the LCD screen and positions the cursor in the upper-left corner.

}

void loop() {
  delay(1000); //start up for temp sensor
  readTemp();
  drawLCD();
  delay(1000);
}

void drawMsg(char msg[], int sz){
  for ( int pos = 0; pos < sz; pos++){
      lcd.scrollDisplayLeft();  //Scrolls the contents of the display one space to the left.
      lcd.print(msg[pos]);  // Print a message to the LCD.
      delay(tim);  //wait for 250 microseconds
      lcd.scrollDisplayLeft();  //Scrolls the contents of the display one space to the left.
  }
}

/************************************************************/


